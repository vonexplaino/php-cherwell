<?php
namespace Cherwell;

include_once 'SearchObject.php';

use \Cherwell\SearchObject;

/**
 * undocumented class
 *
 * @package default
 * @author
 **/
class SearchExportObject extends \Cherwell\SearchObject
{
    public function setCustomSeparator($custom_separator)
    {
        $this->search_object['customSeparator'] = $custom_separator;
    }
    public function setExportFormat($export_format)
    {
        $this->search_object['exportFormat'] = $export_format;
    }
    public function setExportTitle($export_title)
    {
        $this->search_object['exportTitle'] = $export_title;
    }
    public function getJSON()
    {
        return json_encode($this->search_object);
    }
    public function __toString()
    {
        return $this->getJSON();
    }
}
