<?php

namespace Cherwell;

/**
 * undocumented class
 *
 * @package default
 * @author
 **/
class BusinessObjectTemplateRequestObject
{
    private $object = ['includeRequired'=>true, 'includeAll'=>true];

    public function setBusObId($bus_ob_id)
    {
        $this->object['busObId'] = $bus_ob_id;
        return $this;
    }
    public function setIncludeRequired($include_required)
    {
        $this->object['includeRequired'] = $include_required;
        return $this;
    }
    public function setIncludeAll($include_all)
    {
        $this->object['includeAll'] = $include_all;
        return $this;
    }
    public function addFieldName($field_name)
    {
        if (!isset($this->object['fieldNames']) || !in_array($field_name, $this->object['fieldNames'])) {
            $this->object['fieldNames'][] = $field_name;
        }
        return $this;
    }
    public function getJSON()
    {
        return json_encode($this->object);
    }
    public function __toString()
    {
        return $this->getJSON();
    }
} // END class SearchObject
