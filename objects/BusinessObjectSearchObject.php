<?php

namespace Cherwell;

/**
 * undocumented class
 *
 * @package default
 * @author
 **/
class BusinessObjectSearchObject
{
/*
    {
    "filters": [
    {
      "fieldId": "string",
      "operator": "string",
      "value": "string"
    }
    ],
    "stopOnError": true,
    "readRequests": [
    {
      "busObId": "string",
      "busObRecId": "string",
      "busObPublicId": "string"
    }
    ]
    }
    */
    private $search_object = [];


    public function addFilter($filter)
    {
        if (!isset($this->search_object['filters']) || !in_array($filter, $this->search_object['filters'])) {
            $this->search_object['filters'][] = $filter;
        }
    }
    public function setStopOnError($stop_on_error)
    {
        $this->search_object['stopOnError'] = $stop_on_error;
    }
    public function addReadRequest($read_request)
    {
        if (!isset($this->search_object['readRequests']) || !in_array($read_request, $this->search_object['readRequests'])) {
            $this->search_object['readRequests'][] = $read_request;
        }
    }
    public function getJSON()
    {
        return json_encode($this->search_object);
    }
    public function __toString()
    {
        return $this->getJSON();
    }
} // END class SearchObject
