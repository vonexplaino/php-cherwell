# README #

This library is self contained and can be included in whatever library structure you require. It has zero dependencies, whereas the test script has a dependancy on PHPUnit and whatthejeff/nyancat-phpunit-resultprinter for delight reasons.

### What is this repository for? ###

* This is a PHP interface to the [Cherwell service desk REST api](http://griffith.demo.cherwell.com/CherwellAPI/Swagger/ui/index#!/Security/Security_GetSecurityGroupsV1)
* Version 0.1

### How do I get set up? ###

* Download the repository
* Add it to your autoload.php

#### How to run tests ####
1. Install PHPUnit if you haven't already
1. Grab the whatthejeff/nyancat-phpunit-resultprinter repository via composer (install it globally if you can, or make a vendor dir in here)
    1. You could just delete that from the phpunit.xml if you don't like Nyancat.
1. Run `phpunit` in the tests directory. It will create a report dir for code coverage (if you have xdebug installed).

### Contribution guidelines ###

1. Writing tests encouraged.
    # Tests are written against a impersonating API, where the test tells the API what to return before #ch call so we don't hammer Cherwell's servers. 
    # To test the callCurl function, ServiceTest.php starts up php's internal server so the curl calls #e real. If you have a server running on port 8000, you'll need to change that in ServiceTest.php
    # While there is 100% test coverage, the individual objects do not have parameter screening (e.g. #suring that includeLinks is a boolean). That's a todo.
    # Some of the Cherwell APIs don't appear to work as advertised. Especially the attachment. Having someone from Cherwell help would be ever so useful.
1. Code is written to PSR standard, correct where I'm wrong please.

### Use of functions ###

Some examples of functionality.

#### Uploading attachments ####

```php
$result = $api->uploadBusinessObjectAttachmentByBusinessObjectIdAndRecId(
      file_get_contents($filename), 
      str_replace(' ', '_', $filename), 
      $busObId, 
      $busObRecId', 
      0, 
      filesize($filename)
    );
// then
file_put_contents($filename, $api->getBusinessObjectAttachmentByRecId($attachmentId, $busObId, $busObRecId));
```

#### Finding things ####

```php
$search = new \Cherwell\SearchObject();
$search->setBusObId($busObId);
$search->addFilter($api->makeFilter($busObPublicIdFieldID, 'eq', $publicId));
$result = $api->getSearchResults($search);
```