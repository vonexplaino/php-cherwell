<?php

namespace Cherwell;

class HelpersTest extends \PHPUnit_Framework_TestCase
{
    /**
     *
     */
    public function testMakeBusinessReadRequest()
    {

        $api = $this->setupDummyApi();
        try {
            $ok = $api->makeBusinessReadRequest(1, 2, 3);
            $this->assertTrue(false, "Failed to reject a bad business object id");
        } catch (\Exception $e) {
            $this->assertTrue($e->getMessage() === "Business object id must be a string", "Bad error for non-string Business object id");
        }
        try {
            $ok = $api->makeBusinessReadRequest('1', 2, 3);
            $this->assertTrue(false, "Failed to reject a bad business object record id");
        } catch (\Exception $e) {
            $this->assertTrue($e->getMessage() === "Business object record id must be a string", "Bad error for non-string Business object record id");
        }
        try {
            $ok = $api->makeBusinessReadRequest('1', '2', 3);
            $this->assertTrue(false, "Failed to reject a bad business object public id");
        } catch (\Exception $e) {
            $this->assertTrue($e->getMessage() === "Business object public id must be a string", "Bad error for non-string Business object public id");
        }
        $ok = $api->makeBusinessReadRequest('1', '2', '3');
        $this->assertTrue(['busObId'=>'1', 'busObRecId' => '2', 'busObPublicId' => '3'] === $ok);
    }

    private function setupDummyApi()
    {
        return new \Cherwell\CherwellTestWrapper('https://localhost/', 'x', 1);
    }
    public function __construct()
    {
        include_once 'CherwellTestWrapper.php';
    }
}
