<?php

namespace Cherwell;

class ObjectsTest extends \PHPUnit_Framework_TestCase
{
    /**
     *
     */
    public function testAttachmentRequestObject()
    {
        include_once '../objects/AttachmentRequestObject.php';
        $attachment_request_object = new \Cherwell\AttachmentRequestObject();
        $attachment_request_object->setAttachmentId('string');
        $attachment_request_object->addAttachmentType('Imported');
        $attachment_request_object->addAttachmentType('File');
        $attachment_request_object->setBusObId('string');
        $attachment_request_object->setBusObName('string');
        $attachment_request_object->setBusObPublicId('string');
        $attachment_request_object->setBusObRecId('string');
        $attachment_request_object->setIncludeLinks(true);
        $attachment_request_object->addType('None');
        $attachment_request_object->addType('File');
        $string = $attachment_request_object->__toString();
        $json = $attachment_request_object->getJSON();
        $expected = json_encode((object)[
          "attachmentId" => "string",
          "attachmentTypes" => [
            "Imported", "File"
          ],
          "busObId" => "string",
          "busObName" => "string",
          "busObPublicId" => "string",
          "busObRecId" => "string",
          "includeLinks" => true,
          "types" => [
            "None","File"
          ]
        ]);
        $this->assertTrue($string === $expected, "__toString does not match expected output " . var_export($string, true), var_export($expected, true));
        $this->assertTrue($json === $expected, "getJSON does not match expected output");
    }

    /**
     *
     */
    public function testBusinessObjectAttachmentBusinessObjectObject()
    {
        include_once '../objects/BusinessObjectAttachmentBusinessObjectObject.php';
        $object = new \Cherwell\BusinessObjectAttachmentBusinessObjectObject();
        $object->setAttachBusObId('string');
        $object->setAttachBusObName('string');
        $object->setAttachBusObPublicId('string');
        $object->setAttachBusObRecId('string');
        $object->setBusObId('string');
        $object->setBusObName('string');
        $object->setBusObPublicId('string');
        $object->setBusObRecId('string');
        $object->setComment('string');
        $object->setIncludeLinks(true);
        $string = $object->__toString();
        $json = $object->getJSON();
        $expected = json_encode((object)[
            "attachBusObId" => "string",
            "attachBusObName" => "string",
            "attachBusObPublicId" => "string",
            "attachBusObRecId" => "string",
            "busObId" => "string",
            "busObName" => "string",
            "busObPublicId" => "string",
            "busObRecId" => "string",
            "comment" => "string",
            "includeLinks" => true
        ]);
        $this->assertTrue($string === $expected, "__toString does not match expected output " . var_export($string, true), var_export($expected, true));
        $this->assertTrue($json === $expected, "getJSON does not match expected output");
    }
    /**
     *
     */
    public function testBusinessObjectAttachmentLinkObject()
    {
        include_once '../objects/BusinessObjectAttachmentLinkObject.php';
        $object = new \Cherwell\BusinessObjectAttachmentLinkObject();
        $object->setBusObId('string');
        $object->setBusObName('string');
        $object->setBusObPublicId('string');
        $object->setBusObRecId('string');
        $object->setComment('string');
        $object->setDisplayText('string');
        $object->setIncludeLinks(true);
        $object->setUncFilePath('string');
        $string = $object->__toString();
        $json = $object->getJSON();
        $expected = json_encode((object)[
                "busObId" => "string",
                "busObName" => "string",
                "busObPublicId" => "string",
                "busObRecId" => "string",
                "comment" => "string",
                "displayText" => "string",
                "includeLinks" => true,
                "uncFilePath" => "string"
        ]);
        $this->assertTrue($string === $expected, "__toString does not match expected output " . var_export($string, true), var_export($expected, true));
        $this->assertTrue($json === $expected, "getJSON does not match expected output");
    }
    /**
     *
     */
    public function testBusinessObjectAttachmentUrlObject()
    {
        include_once '../objects/BusinessObjectAttachmentUrlObject.php';
        $object = new \Cherwell\BusinessObjectAttachmentUrlObject();
        $object->setBusObId('string');
        $object->setBusObName('string');
        $object->setBusObPublicId('string');
        $object->setBusObRecId('string');
        $object->setComment('string');
        $object->setDisplayText('string');
        $object->setIncludeLinks(true);
        $object->setUrl('string');
        $string = $object->__toString();
        $json = $object->getJSON();
        $expected = json_encode((object)[
                "busObId" => "string",
                "busObName" => "string",
                "busObPublicId" => "string",
                "busObRecId" => "string",
                "comment" => "string",
                "displayText" => "string",
                "includeLinks" => true,
                "url" => "string"
        ]);
        $this->assertTrue($string === $expected, "__toString does not match expected output " . var_export($string, true), var_export($expected, true));
        $this->assertTrue($json === $expected, "getJSON does not match expected output");
    }
    /**
     *
     */
    public function testBusinessObjectFieldsObject()
    {
        include_once '../objects/BusinessObjectFieldsObject.php';
        $object = new \Cherwell\BusinessObjectFieldsObject();
        $api = $this->setupDummyApi();
        $object->setBusObId('string');
        $object->setBusbPublicId('string');
        $object->setBusObRecId('string');
        $object->setFieldId('string');
        $object->addField($api->makeIncomingField('string', 'string', 'string', true));
        $string = $object->__toString();
        $json = $object->getJSON();
        $expected = json_encode((object)[
                "busObId" => "string",
                "busbPublicId" => "string",
                "busObRecId" => "string",
                "fieldId" => "string",
                "fields" => [ [ "fieldId" => "string", "name" => "string", "value" => "string", "dirty" => true ] ]
        ]);
        $this->assertTrue($string === $expected, "__toString does not match expected output ");
        $this->assertTrue($json === $expected, "getJSON does not match expected output");
    }
    /**
     *
     */
    public function testBusinessObjectSaveObject()
    {
        include_once '../objects/BusinessObjectSaveObject.php';
        $object = new \Cherwell\BusinessObjectSaveObject();
        $api = $this->setupDummyApi();
        $object->setBusObId('string');
        $object->setBusObRecId('string');
        $object->setBusObPublicId('string');
        $object->addField($api->makeIncomingField('string', 'string', 'string', true));
        $string = $object->__toString();
        $json = $object->getJSON();
        $returned_object = $object->getObject();
        $expected = (object)[
                "busObId" => "string",
                "busObRecId" => "string",
                "busObPublicId" => "string",
                "fields" => [ [ "fieldId" => "string", "name" => "string", "value" => "string", "dirty" => true ] ]
        ];
        $this->assertTrue($string === json_encode($expected), "__toString does not match expected output ");
        $this->assertTrue($json === json_encode($expected), "getJSON does not match expected output ");
        $this->assertTrue($returned_object == $expected, "getObject does not match expected output ");
    }
    /**
     *
     */
    public function testBusinessObjectSearchObject()
    {
        include_once '../objects/BusinessObjectSearchObject.php';
        $object = new \Cherwell\BusinessObjectSearchObject();
        $api = $this->setupDummyApi();
        $object->addFilter($api->makeFilter('string', 'eq', 'string'));
        $object->setStopOnError(true);
        $object->addReadRequest($api->makeBusinessReadRequest('string', 'string', 'string'));
        $string = $object->__toString();
        $json = $object->getJSON();
        $expected = json_encode((object)[
            "filters" => [
            [
              "fieldId" => "string",
              "operator" => "eq",
              "value" => "string"
            ]
            ],
            "stopOnError" => true,
            "readRequests" => [
            [
              "busObId" => "string",
              "busObRecId" => "string",
              "busObPublicId" => "string"
            ]
            ]
        ]);
        $this->assertTrue($string === $expected, "__toString does not match expected output ");
        $this->assertTrue($json === $expected, "getJSON does not match expected output ");
    }
    /**
     *
     */
    public function testBusinessObjectTemplateRequestObject()
    {
        include_once '../objects/BusinessObjectTemplateRequestObject.php';
        $object = new \Cherwell\BusinessObjectTemplateRequestObject();
        $object->setBusObId('string');
        $object->setIncludeRequired(true);
        $object->setIncludeAll(true);
        $object->addFieldName('string');
        $string = $object->__toString();
        $json = $object->getJSON();
        $expected = json_encode((object)[
            "includeRequired" => true,
            "includeAll" => true,
            "busObId" => "string",
            "fieldNames" => [
                "string"
            ]
        ]);
        $this->assertTrue($string === $expected, "__toString does not match expected output " . var_export($string, true) . ' | ' . var_export($expected, true));
        $this->assertTrue($json === $expected, "getJSON does not match expected output ");
    }
    /**
     *
     */
    public function testDeleteObject()
    {
        include_once '../objects/DeleteObject.php';
        $object = new \Cherwell\DeleteObject();
        $api = $this->setupDummyApi();
        $object->setStopOnError(true);
        $object->addDeleteRequest($api->makeBusinessReadRequest('string', 'string', 'string'));
        $string = $object->__toString();
        $json = $object->getJSON();
        $expected = json_encode((object)[
            "stopOnError" => true,
            "deleteRequests" => [
            [
              "busObId" => "string",
              "busObRecId" => "string",
              "busObPublicId" => "string"
            ]
            ]
        ]);
        $this->assertTrue($string === $expected, "__toString does not match expected output " . var_export($string, true) . ' | ' . var_export($expected, true));
        $this->assertTrue($json === $expected, "getJSON does not match expected output ");
    }
    /**
     *
     */
    public function testDeleteUserBatchObject()
    {
        include_once '../objects/DeleteUserBatchObject.php';
        $object = new \Cherwell\DeleteUserBatchObject();
        $object->setStopOnError(true);
        $object->addUserRecordId("string");
        $object->addUserRecordId("string2");
        $string = $object->__toString();
        $json = $object->getJSON();
        $expected = json_encode((object)[
            "stopOnError" => true,
            "userRecordIds" => ["string","string2"]
        ]);
        $this->assertTrue($string === $expected, "__toString does not match expected output " . var_export($string, true) . ' | ' . var_export($expected, true));
        $this->assertTrue($json === $expected, "getJSON does not match expected output ");
    }
    /**
     *
     */
    public function testReadRequestBatchObject()
    {
        include_once '../objects/ReadRequestBatchObject.php';
        $object = new \Cherwell\ReadRequestBatchObject();
        $object->setStopOnError(true);
        $api = $this->setupDummyApi();
        $object->addReadRequest($api->makeReadRequest('string', 'string'));
        $object->addReadRequest($api->makeReadRequest('string2', 'string'));
        $object->addReadRequest($api->makeReadRequest('string', 'string'));
        $string = $object->__toString();
        $json = $object->getJSON();
        $expected = json_encode((object)[
            "stopOnError" => true,
            "readRequests" => [ [ "loginId" => "string", "publicId" => "string" ], [ "loginId" => "string2", "publicId" => "string" ] ]
        ]);
        $this->assertTrue($string === $expected, "__toString does not match expected output " . var_export($string, true) . ' | ' . var_export($expected, true));
        $this->assertTrue($json === $expected, "getJSON does not match expected output ");
    }
    /**
     *
     */
    public function testRelatedBusinessObjectSaveObject()
    {
        include_once '../objects/RelatedBusinessObjectSaveObject.php';
        $object = new \Cherwell\RelatedBusinessObjectSaveObject();
        $api = $this->setupDummyApi("string");
        $object->setParentBusObId("string");
        $object->setParentBusObPublicId("string");
        $object->setParentBusObRecId("string");
        $object->setRelationshipId("string");
        $object->setBusObId("string");
        $object->setBusObRecId("string");
        $object->setBusObPublicId("string");
        $object->addField($api->makeIncomingField('string', 'string', 'string', true));
        $string = $object->__toString();
        $json = $object->getJSON();
        $expected = json_encode((object)[
                "parentBusObId" => "string",
                "parentBusObPublicId" => "string",
                "parentBusObRecId" => "string",
                "relationshipId" => "string",
                "busObId" => "string",
                "busObRecId" => "string",
                "busObPublicId" => "string",
                "fields" => [
                    [
                      "fieldId" => "string",
                      "name" => "string",
                      "value" => "string",
                      "dirty" => true
                    ]
                ]
        ]);
        $this->assertTrue($string === $expected, "__toString does not match expected output\n" . var_export($string, true) . "\n" . var_export($expected, true));
        $this->assertTrue($json === $expected, "getJSON does not match expected output ");
    }
    /**
     *
     */
    public function testRelatedBusinessObjectSearchObject()
    {
        include_once '../objects/RelatedBusinessObjectSearchObject.php';
        $object = new \Cherwell\RelatedBusinessObjectSearchObject();
        $api = $this->setupDummyApi();
        $object->setAllFields(true);
        $object->setCustomGridId("string");
        $object->addFieldList("string");
        $object->addFilter($api->makeFilter('string', 'lt', 'string'));
        $object->setPageNumber(0);
        $object->setPageSize(0);
        $object->setParentBusObId("string");
        $object->setParentBusObRecId("string");
        $object->setRelationshipId("string");
        $object->setUseDefaultGrid(true);
        $string = $object->__toString();
        $json = $object->getJSON();
        $expected = json_encode((object)[
            "pageNumber" => 0,
            "allFields" => true,
            "customGridId" => "string",
            "fieldsList" => [
                "string"
            ],
            "filters" => [
                [
                  "fieldId" => "string",
                  "operator" => "lt",
                  "value" => "string"
                ]
            ],
            "pageSize" => 0,
            "parentBusObId" => "string",
            "parentBusObRecId" => "string",
            "relationshipId" => "string",
            "useDefaultGrid" => true
        ]);
        $this->assertTrue($string === $expected, "__toString does not match expected output\n" . var_export($string, true) . "\n" . var_export($expected, true));
        $this->assertTrue($json === $expected, "getJSON does not match expected output ");
    }
    /**
     *
     */
    public function testSaveBatchObject()
    {
        include_once '../objects/SaveBatchObject.php';
        $object = new \Cherwell\SaveBatchObject();
        $api = $this->setupDummyApi();
        $object->setStopOnError(true);
        try {
            $object->addSaveRequest('x');
            $this->assertTrue(false, "Failed to reject a non-object save request");
        } catch (\Exception $e) {
        }
        include_once '../objects/BusinessObjectSaveObject.php';
        $to_save = new \Cherwell\BusinessObjectSaveObject();
        $to_save->setBusObId('string');
        $to_save->setBusObRecId('string');
        $to_save->setBusObPublicId('string');
        $to_save->addField($api->makeIncomingField('string', 'string', 'string', true));
        $api = $this->setupDummyApi();
        $object->setStopOnError(true);
        $one = $object->addSaveRequest($to_save);
        $to_save2 = clone $to_save;
        $to_save2->setBusObPublicId('string2');
        $two = $object->addSaveRequest($to_save2);
        $to_save3 = clone $to_save;
        $to_save3->setBusObPublicId('string3');
        $three = $object->addSaveRequest($to_save3);
        $string = $object->__toString();
        $json = $object->getJSON();
        $expected = json_encode((object)[
            "stopOnError" => true,
            "saveRequests" => [ [ "busObId" => "string", "busObRecId" => "string", "busObPublicId" => "string", "fields" => [ [ "fieldId" => "string", "name" => "string", "value" => "string", "dirty" => true ] ] ], [ "busObId" => "string", "busObRecId" => "string", "busObPublicId" => "string2", "fields" => [ [ "fieldId" => "string", "name" => "string", "value" => "string", "dirty" => true ] ] ], [ "busObId" => "string", "busObRecId" => "string", "busObPublicId" => "string3", "fields" => [ [ "fieldId" => "string", "name" => "string", "value" => "string", "dirty" => true ] ] ] ]
        ]);
        $this->assertTrue($string === $expected, "__toString does not match expected output\n" . var_export($string, true) . "\n" . var_export($expected, true));
        $this->assertTrue($json === $expected, "getJSON does not match expected output ");
    }
    /**
     *
     */
    public function testSearchExportObject()
    {
        include_once '../objects/SearchExportObject.php';
        $object = new \Cherwell\SearchExportObject();
        $api = $this->setupDummyApi();
        $object->setCustomSeparator(',');
        $object->setExportFormat('csv');
        $object->setExportTitle('string');
        $object->setAssociation('string');
        $object->setBusObId('string');
        $object->addField('string');
        $object->setIncludeAllFields(true);
        $object->setCustomGridDefId('string');
        $object->setDateTimeFormatting('string');
        $object->setFieldId('string');
        $object->addFilter($api->makeFilter('string', 'gt', 'string'));
        $object->setIncludeSchema(true);
        $object->setPageNumber(0);
        $object->setPageSize(0);
        $object->setScope('string');
        $object->setScopeOwner('string');
        $object->setSearchId('string');
        $object->setSearchName('string');
        $object->setSearchText('string');
        $object->addSorting($api->makeSort('string', 0));
        $string = $object->__toString();
        $json = $object->getJSON();
        $expected = json_encode((object)[
            "customSeparator" => ",",
            "exportFormat" => "csv",
            "exportTitle" => "string",
            "association" => "string",
            "busObId" => "string",
            "fields" => [
                "string"
            ],
            "includeAllFields" => true,
            "customGridDefId" => "string",
            "dateTimeFormatting" => "string",
            "fieldId" => "string",
            "filters" => [
                [
                  "fieldId" => "string",
                  "operator" => "gt",
                  "value" => "string"
                ]
            ],
            "includeSchema" => true,
            "pageNumber" => 0,
            "pageSize" => 0,
            "scope" => "string",
            "scopeOwner" => "string",
            "searchId" => "string",
            "searchName" => "string",
            "searchText" => "string",
            "sorting" => [
                [
                  "fieldId" => "string",
                  "sortDirection" => 0
                ]
            ]
        ]);
        $this->assertTrue($string === $expected, "__toString does not match expected output\n" . var_export($string, true) . "]\n[" . var_export($expected, true));
        $this->assertTrue($json === $expected, "getJSON does not match expected output ");
    }
    /**
     *
     */
    public function testSearchObject()
    {
        include_once '../objects/SearchObject.php';
        $object = new \Cherwell\SearchObject();
        $api = $this->setupDummyApi();
        $object->setAssociation('string');
        $object->setBusObId('string');
        $object->addField('string');
        $object->setIncludeAllFields(true);
        $object->setCustomGridDefId('string');
        $object->setDateTimeFormatting('string');
        $object->setFieldId('string');
        $object->addFilter($api->makeFilter('string', 'gt', 'string'));
        $object->setIncludeSchema(true);
        $object->setPageNumber(0);
        $object->setPageSize(0);
        $object->setScope('string');
        $object->setScopeOwner('string');
        $object->setSearchId('string');
        $object->setSearchName('string');
        $object->setSearchText('string');
        $object->addSorting($api->makeSort('string', 0));
        $string = $object->__toString();
        $json = $object->getJSON();
        $expected = json_encode((object)[
            "association" => "string",
            "busObId" => "string",
            "fields" => [
                "string"
            ],
            "includeAllFields" => true,
            "customGridDefId" => "string",
            "dateTimeFormatting" => "string",
            "fieldId" => "string",
            "filters" => [
                [
                  "fieldId" => "string",
                  "operator" => "gt",
                  "value" => "string"
                ]
            ],
            "includeSchema" => true,
            "pageNumber" => 0,
            "pageSize" => 0,
            "scope" => "string",
            "scopeOwner" => "string",
            "searchId" => "string",
            "searchName" => "string",
            "searchText" => "string",
            "sorting" => [
                [
                  "fieldId" => "string",
                  "sortDirection" => 0
                ]
            ]
        ]);
        $this->assertTrue($string === $expected, "__toString does not match expected output\n" . var_export($string, true) . "\n" . var_export($expected, true));
        $this->assertTrue($json === $expected, "getJSON does not match expected output ");
    }
    /**
     *
     */
    public function testUserObject()
    {
        include_once '../objects/UserObject.php';
        $object = new \Cherwell\UserObject();
        $api = $this->setupDummyApi();
        $object->setAccountLocked(true);
        $object->setBusObId('string');
        $object->setBusObPublicId('string');
        $object->setBusObRecId('string');
        $object->setDisplayName('string');
        $object->setError('string');
        $object->setErrorCode('string');
        $object->setHasError(true);
        $object->setLdapRequired(true);
        $object->setLoginId('string');
        $object->setNextPasswordResetDate('2016-07-27T01 =>02 =>51.172Z');
        $object->setPassword('string');
        $object->setPasswordNeverExpires(true);
        $object->setSecurityGroupId('string');
        $object->setUserCannotChangePassword(true);
        $object->addInfoField($api->makeIncomingField('string', 'string', 'string', true));
        $object->setUserMustChangePasswordAtNextLogin(true);
        $object->setWindowsUserId('string');
        $string = $object->__toString();
        $json = $object->getJSON();
        $expected = json_encode((object)[
            "accountLocked" => true,
            "busObId" => "string",
            "busObPublicId" => "string",
            "busObRecId" => "string",
            "displayName" => "string",
            "error" => "string",
            "errorCode" => "string",
            "hasError" => true,
            "ldapRequired" => true,
            "loginId" => "string",
            "nextPasswordResetDate" => "2016-07-27T01 =>02 =>51.172Z",
            "password" => "string",
            "passwordNeverExpires" => true,
            "securityGroupId" => "string",
            "userCannotChangePassword" => true,
            "userInfoFields" => [
                [
                  "fieldId" => "string",
                  "name" => "string",
                  "value" => "string",
                  "dirty" => true
                ]
            ],
            "userMustChangePasswordAtNextLogin" => true,
            "windowsUserId" => "string"
        ]);
        $this->assertTrue($string === $expected, "__toString does not match expected output\n" . var_export($string, true) . "]\n[" . var_export($expected, true));
        $this->assertTrue($json === $expected, "getJSON does not match expected output ");
    }

    private function setupDummyApi()
    {
        return new \Cherwell\CherwellTestWrapper('https://localhost/', 'x', 1);
    }
    public function __construct()
    {
        include_once 'CherwellTestWrapper.php';
    }
}
